﻿using Cards.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Cards.Models
{
    public class CardModel
    {
        public enum CardEventMethod
        {
            Add,
            Update
        }

        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public int position { get; set; }
        public string asigneeId { get; set; }

        public CardModel()
        {
        }

        public CardModel(
             string title,
             string description
        )
        {
            this.title = title;
            this.description = description;
        }

        public CardModel(
            string id,
            string title,
            string description,
            int status,
            int position,
            string asigneeId
        )
        {
            this.id = id;
            this.title = title;
            this.description = description;
            this.status = status;
            this.position = position;
            this.asigneeId = asigneeId;
        }

        public Dictionary<string, string> ToDictionary(
            CardEventMethod method
        )
        {
            var dictionary = new Dictionary<string, string>();
            switch(method)
            {
                case CardEventMethod.Add:
                    dictionary.Add("title", this.title);
                    dictionary.Add("description", this.description);
                    break;
                case CardEventMethod.Update:
                    dictionary.Add("id", this.id);
                    dictionary.Add("title", this.title);
                    dictionary.Add("description", this.description);
                    dictionary.Add("status", this.status.ToString());
                    dictionary.Add("position", this.position.ToString());
                    dictionary.Add("asigneeId", this.asigneeId);
                    break;
            }
            return dictionary;
        }

        public static bool AddCard(
            string email,
            string password,
            CardModel card
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
                var resultStream = APIServices.SendData(
                    APIServices.MethodTypes.POST,
                    "Cards/Add",
                    card.ToDictionary(
                        CardModel.CardEventMethod.Add
                    )
                );
            }
            return true;
        }

        public static bool UpdateCard(
            string email,
            string password,
            CardModel card
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
                var resultStream = APIServices.SendData(
                    APIServices.MethodTypes.POST,
                    "Cards/Update",
                    card.ToDictionary(
                        CardModel.CardEventMethod.Update
                    )
                );
            }
            int[] res = new int[10];

            return true;
        }

        public static bool DeleteCard(
            string email,
            string password,
            string cardId
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
            /*    var data = new Dictionary<string, string>();
                var resultStream = APIServices.SendPost("/Cards/Delete", data);*/
            }
            int[] res = new int[10];

            return true;
        }

        public static Array GetAll(
            string email,
            string password
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
                var resultStream = APIServices.SendData(
                    APIServices.MethodTypes.GET,
                    "Cards/GetAll",
                    null
                );
                if(resultStream != null)
                {
                    var cardArray = JsonSerializer.Deserialize<CardModel[]>(
                        resultStream
                    );
                    return cardArray;
                }
            }
            return new CardModel[0];
        }

        public static string GetCardData(
           string email,
           string password
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
              /*  var data = new Dictionary<string, string>();
                var resultStream = APIServices.SendPost("/Cards/GetById", data);*/
            }
            int[] res = new int[10];

            return "";
        }
        public static string CardPosition(
          string email,
          string password
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
             /*   var data = new Dictionary<string, string>();
                var resultStream = APIServices.SendPost("/Cards/GetAll", data);*/
            }
            int[] res = new int[10];

            return "";
        }

        public static string CardMove(
            string email,
            string password
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
            /*    var data = new Dictionary<string, string>();
                var resultStream = APIServices.SendPost("/Cards/GetAll", data);*/
            }
            int[] res = new int[10];

            return "";
        }
      
        public static string CardUserBind(
            string email,
            string password
        )
        {
            if (APIServices.AuthGateway(email, password))
            {
              /*  var data = new Dictionary<string, string>();
                var resultStream = APIServices.SendPost("/Cards/GetAll", data);*/
            }
            int[] res = new int[10];

            return "";
        }
    }
}
