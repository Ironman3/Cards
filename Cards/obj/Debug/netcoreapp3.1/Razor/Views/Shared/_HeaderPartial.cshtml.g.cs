#pragma checksum "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\Shared\_HeaderPartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3a9ab97d02c1866393c7cd3e5fcec9e69d4f10c0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__HeaderPartial), @"mvc.1.0.view", @"/Views/Shared/_HeaderPartial.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\_ViewImports.cshtml"
using Cards;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\_ViewImports.cshtml"
using Cards.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3a9ab97d02c1866393c7cd3e5fcec9e69d4f10c0", @"/Views/Shared/_HeaderPartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"218d696472f75d68fed934370ccee98db1099256", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__HeaderPartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div class=\"card-container-column-header\">\r\n    ");
#nullable restore
#line 7 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\Shared\_HeaderPartial.cshtml"
Write(ViewData["HeaderTitle"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    <div class=\"form\">\r\n        <div class=\"row\">\r\n            <div class=\"col-12\">\r\n                <label");
            BeginWriteAttribute("for", " for=\"", 311, "\"", 346, 2);
            WriteAttributeValue("", 317, "title_", 317, 6, true);
#nullable restore
#line 11 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\Shared\_HeaderPartial.cshtml"
WriteAttributeValue("", 323, ViewData["HeaderType"], 323, 23, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Title</label>\r\n                <input");
            BeginWriteAttribute("id", " id=\"", 385, "\"", 419, 2);
            WriteAttributeValue("", 390, "title_", 390, 6, true);
#nullable restore
#line 12 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\Shared\_HeaderPartial.cshtml"
WriteAttributeValue("", 396, ViewData["HeaderType"], 396, 23, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" name=\"title\" class=\"form-control\" />\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-12\">\r\n                <label");
            BeginWriteAttribute("for", " for=\"", 578, "\"", 619, 2);
            WriteAttributeValue("", 584, "description_", 584, 12, true);
#nullable restore
#line 17 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\Shared\_HeaderPartial.cshtml"
WriteAttributeValue("", 596, ViewData["HeaderType"], 596, 23, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Description</label>\r\n                <input");
            BeginWriteAttribute("id", " id=\"", 664, "\"", 704, 2);
            WriteAttributeValue("", 669, "description_", 669, 12, true);
#nullable restore
#line 18 "C:\Users\Iron Illustration\source\repos\Cards\Cards\Views\Shared\_HeaderPartial.cshtml"
WriteAttributeValue("", 681, ViewData["HeaderType"], 681, 23, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" name=""description"" class=""form-control"" />
            </div>
        </div>
        <div class=""row"">
            <div class=""col-12"">
                <button class=""btn btn-primary event-add-card"">Add</button>
            </div>
        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
