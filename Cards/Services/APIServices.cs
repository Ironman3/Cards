﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Cards.Services
{
    public class APIServices
    {
        private static HttpContext _httpContext => new HttpContextAccessor().HttpContext;

        public enum MethodTypes
        {
            GET,
            POST,
            PUT,
            DEL
        }

        private static string baseURL = "http://193.201.187.29:84/";
        public static string SendData(
            MethodTypes type,
            string target,
            object data
        )
        {
            string url = (
                APIServices.baseURL + target
            );
            WebRequest webRequest = WebRequest.Create(
                url
            );
            //---
            if (_httpContext.Session.Keys.Contains("api_bearer_token"))
            {
                var bearerToken = _httpContext.Session.GetString(
                    "api_bearer_token"
                );
                webRequest.Headers.Add(
                    "Authorization",
                    "Bearer " + bearerToken
                );
            }
            //---
            webRequest.Method = type.ToString();
            if (type == MethodTypes.POST || type == MethodTypes.PUT)
            {
                if (data == null)
                {
                    data = new Dictionary<string, string>();
                }
                var dataStream = JsonSerializer.Serialize(
                    data
                );
                webRequest.ContentLength = dataStream.Length;
                webRequest.ContentType = "application/json; charset=UTF-8";
                using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    streamWriter.Write(dataStream);
                }
            }
            try
            {
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                Stream receiveStream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                //---
                return content;
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
                return null;
            }
        }
        public static bool AuthGateway(
            string email,
            string password
        )
        {
            if (_httpContext.Session.Keys.Contains("api_bearer_token"))
            {
                var expireAt = Convert.ToDateTime(
                    _httpContext.Session.GetString("api_expires_at")
                ).Ticks;
                if (DateTime.Now.Ticks > expireAt)
                {
                    _httpContext.Session.Remove("api_bearer_token");
                    return APIServices.AuthGateway(email, password);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                var target = "Authentication/SignIn";
                var dataMap = new Dictionary<string, string>();
                dataMap.Add("email", email);
                dataMap.Add("password", password);
                string resultStream = APIServices.SendData(
                    APIServices.MethodTypes.POST,
                    target,
                    dataMap
                );
                if (resultStream != null)
                {
                    var result = JsonSerializer.Deserialize<Dictionary<string, string>>(
                        resultStream
                    );
                    _httpContext.Session.SetString(
                        "api_bearer_token",
                        result["token"]
                    );
                    _httpContext.Session.SetString(
                        "api_expires_at",
                        result["expiresAt"]
                    );
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
